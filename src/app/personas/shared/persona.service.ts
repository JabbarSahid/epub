import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, combineLatest } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';

import { PersonaId, Persona } from './persona';

@Injectable({
  providedIn: 'root'
})
export class PersonaService {

  // Nombre de la coleccion
  collectionName: string = 'personas';

  // Current document id
  private _id$: BehaviorSubject<string> = new BehaviorSubject('0');
  set id(id: string) { this._id$.next(id) }
  get id() { return this._id$.value }

  // Current loaded collection
  col$: Observable<PersonaId[]>;
  doc$: BehaviorSubject<any | null> = new BehaviorSubject(this.createEmpty());

  constructor(
    protected afs: AngularFirestore,
  ) {
    this.initCol();
    this.initDoc();
  }

  /**
   * Init col$
   */
  initCol() {
    this.col$ = this.afs.collection(this.collectionName).snapshotChanges().pipe(
      map(items => {
        let data = this.mapItems(items);
        return data;
      })
    );
  }

  /**
   * Init doc$
   */
  initDoc() {
    combineLatest(
      this._id$
    ).pipe(
      switchMap(([id]) => this.getDocumentReference(id)
        .valueChanges().pipe(
          map(doc => {
            if (undefined != doc) {
              return {id, ...doc}
            } else {
              return doc;
            }
          }) // map
        ) // pipe 2
      ) // switchMap
    ).subscribe(doc => {
      if (undefined != doc) {
        this.doc$.next(doc);
      } else {
        this.doc$.next(this.createEmpty());
      }
    });
  }

  save(data): Promise<any> {
    console.log(data);
    const docData = this.extract(data);
    const colRef = this.afs.collection('personas');

    // Create
    if ('0' != data.id && undefined != data.id) {
      return colRef.doc(data.id).set(docData);
      
    // Update
    } else {
      return colRef.add(docData);
    }
  }

  extract(fd) {
    let data: Persona;
    data = {
      apellido: undefined == fd.apellido ? '' : fd.apellido,
      nombre: undefined == fd.nombre ? '' : fd.nombre,
      telefono: undefined == fd.telefono ? '' : fd.telefono,
    }
    return data;
  }

  /**
   * Create empty object
   */
  createEmpty(): PersonaId {
    return {
      id: '0',
      nombre: '',
      apellido: '',
      telefono: ''
    }
  }

  getDocumentReference(id): AngularFirestoreDocument {
    return this.afs.doc<PersonaId>(this.collectionName + '/' + id);
  }

  /**
   * Map collection items to include id
   * @param items 
   */
  mapItems(items) {
    return items.map(a => {
      const data = a.payload.doc.data() as PersonaId;
      const id = a.payload.doc.id;
      return { id, ...data };
    });
  }
}