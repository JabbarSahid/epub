import { Component, OnInit }  from '@angular/core';
import { Router }             from '@angular/router';

@Component({
  selector: 'app-persona-list',
  templateUrl: './persona-list.component.html',
  styleUrls: ['./persona-list.component.css']
})
export class PersonaListComponent implements OnInit {

  constructor(
    private router: Router,
  ) { }

  ngOnInit() { }

  /**
   * Grid create event handler
   */
  onCreate() {
    this.router.navigate(['/personas', '0']);
  }

  /**
   * Grid select event handler
   * @param $event 
   */
  onSelect($event) {
    this.router.navigate(['/personas', $event.id])
  }
}