import { Component, OnInit } from '@angular/core';

import { AuthService } from '../auth/shared/auth.service';

@Component({
  selector: 'app-empresas',
  templateUrl: './empresas.component.html',
  styleUrls: ['./empresas.component.css']
})
export class EmpresasComponent implements OnInit {

  constructor(
    private auth: AuthService
  ) { }

  ngOnInit() {
  }

  signOut() {
    this.auth.signOut();
  }

}
