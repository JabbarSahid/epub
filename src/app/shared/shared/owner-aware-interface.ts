type Fecha = Date | firebase.firestore.FieldValue | null;

export interface OwnerAwareInterface {
  creador: string;
  creado: Fecha;
  modificador: string;
  modificado: Fecha;
}
