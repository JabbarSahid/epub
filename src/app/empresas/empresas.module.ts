import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmpresasRoutingModule } from './empresas-routing.module';
import { EmpresasComponent } from './empresas.component';
import { EmpresaListComponent } from './empresa-list/empresa-list.component';

import { AuthModule } from '../auth/auth.module';
import { EmpresaGridComponent } from './empresa-grid/empresa-grid.component';
import { EmpresaComponent } from './empresa/empresa.component';
import { EmpresaDetailsComponent } from './empresa-details/empresa-details.component';
import { EmpresaFormComponent } from './empresa-form/empresa-form.component';

@NgModule({
  declarations: [EmpresasComponent, EmpresaListComponent, EmpresaGridComponent, EmpresaComponent, EmpresaDetailsComponent, EmpresaFormComponent],
  imports: [
    CommonModule,
    EmpresasRoutingModule,
    AuthModule
  ]
})
export class EmpresasModule { }
