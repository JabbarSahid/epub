import { BrowserModule }            from '@angular/platform-browser';
import { NgModule }                 from '@angular/core';
import { AppRoutingModule }         from './app-routing.module';

import { BrowserAnimationsModule }  from "@angular/platform-browser/animations";
import { AppMaterialModule }        from "./app-material/app-material.module";
import { FlexLayoutModule }         from "@angular/flex-layout";

import { AngularFireModule }        from "@angular/fire";
import { AngularFirestoreModule, 
  FirestoreSettingsToken }          from "@angular/fire/firestore";
import { AngularFireStorageModule } from "@angular/fire/storage";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { AngularFireFunctionsModule } from "@angular/fire/functions";
import { environment } from "../environments/environment";

import { AppComponent }     from './app.component';
import { CoreModule }       from './core/core.module';
import { AuthModule } from './auth/auth.module';

export const firebaseConfig = environment.firebase;

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AppMaterialModule,
    FlexLayoutModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireAuthModule,
    AngularFireFunctionsModule,
    CoreModule,
    AuthModule
  ],
  providers: [
    { provide: FirestoreSettingsToken, useValue: {} }
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
