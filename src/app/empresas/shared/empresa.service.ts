import { Injectable, OnDestroy } from '@angular/core';
import { AbstractFirestoreService } from 'src/app/core/abstract/abstract-firestore.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable, BehaviorSubject } from 'rxjs';
import * as _ from "lodash";

import { EmpresaId, Empresa } from './empresa';
import { AuthService } from 'src/app/auth/shared/auth.service';

@Injectable({
  providedIn: 'root'
})
export class EmpresaService extends AbstractFirestoreService implements OnDestroy {

  collectionName: string = 'empresas';
  col$: Observable<EmpresaId[]>;
  doc$: BehaviorSubject<EmpresaId | null> = new BehaviorSubject(this.createEmpty());

  constructor(
    protected afs: AngularFirestore,
    protected auth: AuthService
  ) {
    super(afs);
  }

  /**
   * Map collection of items into array of objects
   * @param items 
   */
  mapItems(items) {
    return items.map(a => {
      const data = a.payload.doc.data() as EmpresaId;
      const id = a.payload.doc.id;
      return { id, ...data };
    })
  }

  createEmpty(): EmpresaId {
    return {
      id: '0',
      razonSocial: '',
      direccion: '',
      creado: null,
      creador: '',
      modificado: null,
      modificador: ''
    }
  }

  /**
   * 
   * @param fd 
   */
  extract(fd): Empresa {
    return {
      razonSocial: (fd.razonSocial) ? _.trim(fd.razonSocial) : '',
      direccion: (fd.direccion) ? _.trim(fd.direccion) : '',
      creado: (fd.creado && '' !== fd.creado) ? fd.creado : this.timestamp,
      creador: (fd.creador && '' !== fd.creador) ? fd.creador : this.auth.getUid(),
      modificado: this.timestamp,
      modificador: this.auth.getUid()
    }
  }

  ngOnDestroy() { super.ngOnDestroy() }
}
