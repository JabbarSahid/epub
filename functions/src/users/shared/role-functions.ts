/**
 * Add required role to custom claims
 * @param email 
 * @param role 
 */
export async function grantRole(email: string, role: string = 'guest') {
  // Load/init admin
  const admin = await import('firebase-admin');
  try { admin.initializeApp() } catch (e) {}

  // Get roles
  const roles = getRolesObject();

  // Update custom claims
  const user = await admin.auth().getUserByEmail(email);
  return admin.auth().setCustomUserClaims(user.uid, roles);
}

/**
 * Rerturn roles object by role name.
 * This is used for custom claims.
 * @param role 
 */
function getRolesObject(role: string = 'guest') {
  let roles: { [ s: string ]: boolean } = {};
  switch (role) {
    case 'webmaster':
      roles = { webmaster: true, admin: true, operative: true, guest: true };
      break;
    case 'admin':
      roles = { webmaster: false, admin: true, operative: true, guest: true };
      break;
    case 'operative':
      roles = { webmaster: false, admin: false, operative: true, guest: true };
      break;
    case 'guest':
    default:
      roles = { webmaster: false, admin: false, operative: false, guest: true };
      break;
  }
  return roles;
}

/**
 * Returns roles array by role name.
 * This is used to save on database.
 * @param role 
 */
export function getRolesArray(role: string = 'guest') {
  let roles: Array<string> = [];
  switch (role) {
    case 'webmaster':
      roles = [ 'webmaster', 'admin', 'operative', 'guest' ];
      break;
    case 'admin':
      roles = [ 'admin', 'operative', 'guest' ];
      break;
    case 'operative':
      roles = [ 'operative', 'guest' ];
      break;
    case 'guest':
    default:
      roles = [ 'guest' ];
      break;
  }
  return roles;
}