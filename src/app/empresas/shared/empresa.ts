import { OwnerAwareInterface } from 'src/app/shared/shared/owner-aware-interface';

/**
 * Objeto que representa una empresa
 */
export interface Empresa extends OwnerAwareInterface {
  /** Nombre o razón social de la empresa. */
  razonSocial: string;
  /** Dirección de la empresa. */
  direccion: string;
  /** Ciudad de la empresa. */
  ciudad?: string;
}

export interface EmpresaId extends Empresa {
  id: string;
}
