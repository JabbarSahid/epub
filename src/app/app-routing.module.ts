import { NgModule }             from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NotFoundComponent }    from './core/not-found/not-found.component';
import { LoginComponent } from './auth/login/login.component';
import { AuthGuard } from './auth/guard/auth.guard';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'empresas',
    loadChildren: () => import('./empresas/empresas.module').then(m => m.EmpresasModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'personas',
    loadChildren: () => import('./personas/personas.module').then(m => m.PersonasModule)
  },
  {
    path: '',
    loadChildren: () => import('./inicio/inicio.module').then(m => m.InicioModule)
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
