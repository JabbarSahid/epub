import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { AuthService } from '../shared/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private auth: AuthService
  ) { }

  ngOnInit() {
    this.form = this.buildForm();
  }

  buildForm() {
    return this.fb.group({
      email: ['', [
        Validators.required,
        Validators.email
      ]],
      password: ['', [
        Validators.required
      ]]
    });
  }

  async signWithEmailPassword() {
    try {
      await this.auth.emailPasswordLogin(this.email.value, this.password.value);  
    } catch (error) {
      this.password.setValue('');
    } 
  }

  async signWithGoogle() {
    try {
      await this.auth.googleLogin();
    } catch {
      console.log('Falla en la autenticacion.');
    }
  }

  async signWithFacebook() {
    try {
      await this.auth.facebookLogin();
    } catch {
      console.log('Falla en la autenticacion.');
    }
  }

  /**
   * Getters & setters
   */

  get email() {
    return this.form.get('email');
  }

  set email(email) {
    this.email.setValue(email);
  }

  get password() {
    return this.form.get('password');
  }

  set password(password) {
    this.password.setValue(password);
  }
}
