import { Component, OnInit } from '@angular/core';

@Component({})
export abstract class AbstractGridComponent implements OnInit {

  abstract slug: string;

  loading: boolean = true;

  constructor() {

  }

  ngOnInit() {
    
  }

  onLoading() {
    this.loading = true;
  }

  onLoaded() {
    this.loading = false;
  }

}