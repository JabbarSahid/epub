import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { PersonaService } from '../shared/persona.service';

@Component({
  selector: 'app-persona-form',
  templateUrl: './persona-form.component.html',
  styleUrls: ['./persona-form.component.css']
})
export class PersonaFormComponent implements OnInit {

  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private mainService: PersonaService,
  ) { }

  ngOnInit() {
    this.form = this.buildForm();
    this.mainService.doc$.subscribe(doc => {
      this.populateForm(doc);
    })
  }

  buildForm() {
    return this.fb.group({
      id: '0',
      apellido: ['', []],
      nombre: ['', []],
      telefono: ['', []],
    });
  }

  populateForm(doc) {
    if (null !== doc) {
      this.id = doc.id;
      this.apellido = doc.apellido;
      this.nombre = doc.nombre;
      this.telefono = doc.telefono;
    }
  }

  onSave() {
    const fData = this.form.value;
    this.mainService.save(fData)
      .then(doc => {
        console.log(doc);

        // Create
        if (undefined !== doc) {
          this.id = doc.id;
        // Update
        } else {
          
        }
      })
    /*
    this.mainService.save(fData)
      .then(doc => {

        // If creation
        if (undefined !== doc) {
          
        }

        console.log(doc);
      })
      .catch(error => {console.log(error)});
      */
  }

  /**
   * Getters & Setters
   */

   get id() {
     return this.form.get('id');
   }

   set id(id) {
    this.id.setValue(id);
   }

   get apellido() {
    return this.form.get('apellido');
  }

  set apellido(apellido) {
    this.apellido.setValue(apellido);
  }

   get nombre() {
     return this.form.get('nombre');
   }

   set nombre(nombre) {
     this.nombre.setValue(nombre);
   }

   get telefono() {
    return this.form.get('telefono');
  }

  set telefono(telefono) {
    this.telefono.setValue(telefono);
  }
}