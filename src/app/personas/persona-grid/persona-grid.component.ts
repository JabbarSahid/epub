import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';

import { AbstractGridComponent } from 'src/app/core/abstract/abstract-grid.component';
import { PersonaService } from '../shared/persona.service';
import { PersonaId } from '../shared/persona';

@Component({
  selector: 'app-persona-grid',
  templateUrl: './persona-grid.component.html',
  styleUrls: ['./persona-grid.component.css']
})
export class PersonaGridComponent extends AbstractGridComponent implements OnInit, OnDestroy {

  slug = 'personas';

@Input()
set canCreate(canCreate: boolean) { this._canCreate = canCreate }
get canCreate(): boolean { return this._canCreate }
private _canCreate: boolean = false;

@Output() create = new EventEmitter<boolean>();
@Output() select = new EventEmitter<any>();

  // Subscription
  colSub: Subscription;

  personas: Array<PersonaId>;
  dataSource: MatTableDataSource<PersonaId>;

  private _columns: string[] = [ 'nombre', 'apellido', 'telefono' ];
  get columns() {
    return this._columns;
  }

  constructor(
    protected mainService: PersonaService
  ) {
    super();
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngAfterViewInit() {
    this.colSub = this.mainService.col$.subscribe(
      data => {
        this.dataSource = new MatTableDataSource(data);
        this.onLoaded();
      },
      error => {
        this.onLoaded();
      },
    );
  }

  /**
   * Crear click event handler
   */
  onCrearClick() {
    console.log('Click en Crear.');
    this.create.emit(true);
  }

  /**
   * Row click event handler
   * @param row 
   */
  onRowClick(row) {
    console.log(row);
    this.select.emit(row);
  }

  /**
   * @todo: Destruir subscriptions
   */
  ngOnDestroy() {
    this.colSub.unsubscribe();
  }

}