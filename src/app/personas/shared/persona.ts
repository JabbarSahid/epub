export interface Persona {
  nombre: string;
  apellido: string;
  telefono: string;
}

export interface PersonaId extends Persona {
  id: string;
}