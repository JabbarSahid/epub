import { NgModule }                 from '@angular/core';
import { Routes, RouterModule }     from '@angular/router';

import { PersonasComponent }        from './personas.component';
import { PersonaListComponent }     from './persona-list/persona-list.component';
import { PersonaComponent }         from './persona/persona.component';
import { PersonaDetailsComponent }  from './persona-details/persona-details.component';

const routes: Routes = [
  {
    path: '',
    component: PersonasComponent,
    children: [
      {
        path: '',
        component: PersonaListComponent
      },
      {
        path: ':id',
        component: PersonaComponent,
        children: [
          {
            path: '',
            redirectTo: 'detalles'
          },
          {
            path: 'detalles',
            component: PersonaDetailsComponent
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PersonasRoutingModule { }
