import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PersonasRoutingModule } from './personas-routing.module';

import { AppMaterialModule } from "../app-material/app-material.module";
import { FlexLayoutModule } from "@angular/flex-layout";
import { ReactiveFormsModule } from '@angular/forms';

import { PersonasComponent } from './personas.component';
import { PersonaComponent } from './persona/persona.component';
import { PersonaListComponent } from './persona-list/persona-list.component';
import { PersonaGridComponent } from './persona-grid/persona-grid.component';
import { PersonaFormComponent } from './persona-form/persona-form.component';
import { PersonaDetailsComponent } from './persona-details/persona-details.component';
import { PersonaService } from './shared/persona.service';

@NgModule({
  declarations: [
    PersonasComponent, 
    PersonaComponent, 
    PersonaListComponent, 
    PersonaGridComponent, 
    PersonaFormComponent, 
    PersonaDetailsComponent
  ],
  imports: [
    CommonModule,
    PersonasRoutingModule,
    AppMaterialModule,
    FlexLayoutModule,
    ReactiveFormsModule,
  ],
  providers: [
    PersonaService
  ]
})
export class PersonasModule { }
