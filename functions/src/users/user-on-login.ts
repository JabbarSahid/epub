import * as functions from "firebase-functions";
import { grantRole } from "./shared/role-functions";
import { User } from "./shared/user";

export const userOnLogin = functions.https.onCall(async (data, context) => {
  // Validate data
  if (undefined === data.email || '' === data.email || undefined === data.uid || '' === data.uid) {
    return { error: 'Malformed data.' };
  }

  // Load/init admin
  const admin = await import('firebase-admin');
  try { admin.initializeApp() } catch (e) {}
  
  // Clean User
  const cleanData: User = {
    email: data.email,
    displayName: data.displayName || '',
    photoURL: data.photoURL || '',
    lastLogin: admin.firestore.FieldValue.serverTimestamp()
  }

  // If user exists, update it
  const userS = await admin.firestore().collection('users').doc(data.uid).get();
  if (userS.exists) {
    return userS.ref.update(cleanData);

  // If user doesn't exist, create it and grant roles.
  } else {
    // Set uid and role
    cleanData.uid = data.uid;
    cleanData.roles = [ 'guest' ];

    // Set creation data
    cleanData.creado = admin.firestore.FieldValue.serverTimestamp();
    cleanData.creador = data.uid;
    cleanData.modificado = admin.firestore.FieldValue.serverTimestamp();
    cleanData.modificador = data.uid;

    await userS.ref.set(cleanData);
    return grantRole(data.email, 'guest').then(() => {
      return {
        result: `Request fullfilled! ${cleanData.email} is now a webmaster.`
      }
    });
  }
});