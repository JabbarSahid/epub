import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireFunctions } from '@angular/fire/functions';
import { auth } from 'firebase/app';

import { User } from '../../usuarios/shared/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: Observable<User|null>;

  constructor(
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private afFns: AngularFireFunctions,
    private router: Router
  ) {
    // Load user
    this.user = this.afAuth.authState.pipe(
      switchMap(user => {
        if (user) {
          return this.afs.doc<User>(`users/${user.uid}`).valueChanges();
        } else {
          return of(null);
        }
      })
    );

    this.user.subscribe(user => {
      console.log(user);
    })
  }

  /**
   * Return user uid
   */
  getUid(): string {
    return this.afAuth.auth.currentUser.uid;
  }

  /**
   * Login with email and password
   * @param email 
   * @param password 
   */
  emailPasswordLogin(email: string, password: string): Promise<any> {
    return this.afAuth.auth
      .signInWithEmailAndPassword(email, password)
      .then(credential => {
        return this.updateUSerData(credential.user);
      })
      .catch(error => this.handleError(error));
  }

  googleLogin() {
    const provider = new auth.GoogleAuthProvider();
    return this.oAuthLogin(provider);
  }

  facebookLogin() {
    const provider = new auth.FacebookAuthProvider();
    return this.oAuthLogin(provider);
  }

  private oAuthLogin(provider: any) {
    return this.afAuth.auth
      .signInWithPopup(provider)
      .then(credential => {
        return this.updateUSerData(credential.user);
      })
      .catch(error => this.handleError(error));
  }

  /**
   * Update/create user
   * @param user 
   */
  async updateUSerData(user) {
    const data: User = {
      uid: user.uid,
      email: user.email || null,
      displayName: user.displayName || '',
      photoURL: user.photoURL || ''
    }

    const updateUserOnLogin = this.afFns.httpsCallable('userOnLogin');
    const updated = await updateUserOnLogin(data).toPromise();

    if (undefined !== updated.result) {
      await this.afAuth.auth.currentUser.getIdTokenResult(true);
    }

    return;
    /// return this.afs.doc(`users/${user.uid}`).set(data);
  }

  /**
   * Sign out and redirect to login.
   */
  signOut() {
    this.afAuth.auth.signOut()
      .then(() => {
        this.router.navigate(['/login']);
      })
  }

  handleError(error) {
    throw error;
  }
}
