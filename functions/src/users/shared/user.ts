import * as admin from 'firebase-admin';
try { admin.initializeApp() } catch (e) {};
import Timestamp = admin.firestore.Timestamp;
import FieldValue = admin.firestore.FieldValue;

export interface User {
  uid?: string;
  displayName?: string;
  email?: string;
  photoURL?: string;
  roles?: Array<string>;
  lastLogin?: Timestamp | FieldValue | null;

  creado?: Timestamp | FieldValue | null;
  creador?: string;
  modificado?: Timestamp | FieldValue | null;
  modificador?: string;
}

export interface UserId extends User {
  id: string;
}
