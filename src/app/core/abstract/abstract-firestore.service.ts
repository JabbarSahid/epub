import { Injectable, OnDestroy } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable, BehaviorSubject, combineLatest, of, Subscription } from 'rxjs';
import { switchMap, map } from 'rxjs/operators';
import * as firebase from "firebase/app";

@Injectable({
  providedIn: 'root'
})
export abstract class AbstractFirestoreService implements OnDestroy {

  // Collection name
  abstract collectionName: string;
  // Curren loaded collection
  abstract col$: Observable<any[]>;
  // Current loaded document
  abstract doc$: BehaviorSubject<any | null>;

  // Current object id
  set id(id: string) { this._id$.next(id) }
  get id() { return this._id$.value }
  private _id$: BehaviorSubject<string> = new BehaviorSubject('0');

  set pageIndex(pageIndex: number) { this._pageIndex$.next(pageIndex) }
  get pageIndex() { return this._pageIndex$.value }
  private _pageIndex$: BehaviorSubject<number|null> = new BehaviorSubject(0);

  set pageSize(pageSize: number) { this._pageSize$.next(pageSize) }
  get pageSize() { return this._pageSize$.value }
  private _pageSize$: BehaviorSubject<number|null> = new BehaviorSubject(50);

  set sort(sort: string) { this._sort$.next(sort) }
  get sort() { return this._sort$.value }
  private _sort$: BehaviorSubject<string|null> = new BehaviorSubject(null);

  set sortDir(sortDir: string) { this._sortDir$.next(sortDir) }
  get sortDir() { return this._sortDir$.value }
  private _sortDir$: BehaviorSubject<string|null> = new BehaviorSubject(null);

  // Pagination aux vars
  cursor: Array<any> = [];
  itemCount: number = 100;

  subscriptions: Subscription[] = [];

  constructor(
    protected afs: AngularFirestore
  ) { }

  /**
   * Init current collection
   */
  intiCol() {
    /**
     * @todo itemCount from database
     * @todo count function
     */

    this.col$ = combineLatest(
      this._pageIndex$,
      this._pageSize$,
      this._sort$,
      this._sortDir$
    ).pipe(
      switchMap(([pageIndex, pageSize, sort, sortDir]) => 
        this.afs.collection(this.collectionName, ref => {
          let query: firebase.firestore.CollectionReference | firebase.firestore.Query = ref;

          // Sort data
          if (sort) {
            if ('asc' === sortDir) {
              query = query.orderBy(sort, 'asc');
            } else if ('desc' === sortDir) {
              query = query.orderBy(sort, 'desc');
            } else {
              query = query.orderBy(sort, 'asc');
            }
          }

          // Set paging
          if (pageIndex) { query = query.startAfter(this.cursor[pageIndex]) }
          if (pageSize) { query = query.limit(pageSize) }

          return query;
        }).snapshotChanges().pipe(
          map(items => {
            let data = this.mapItems(items);

            // Build next cursor
            if (items.length > 0) {
              this.cursor[pageIndex + 1] = items[items.length -1].payload.doc;
            }

            return data;
          }) // map
        ) // pipe 2
      ) // switchMap
    ) // pipe 1
  }

  initDoc() {
    this.subscriptions.push(
      combineLatest(
        this._id$
      ).pipe(
        switchMap(([id]) => {
          if ('0' === id) {
            return of(this.createEmpty())
          } else {
            return this.afs.collection(this.collectionName).valueChanges().pipe(
              map(doc => {
                if (undefined !== doc) {
                  return {id, ...doc}
                } else {
                  return doc;
                }
              })
            )
          }
        })
      ).subscribe(doc => {
        this.doc$.next(doc);
      })
    )
  }

  /**
   * Save (create/update) a document
   * @param data 
   */
  save(data: any): Promise<any> {
    const collectionRef = this.getCollectionReference();
    const docData: any = this.extract(data);

    // If update
    if ('0' !== data.id && undefined !== data.id) {
      collectionRef.doc(data.id).set(docData);
    // Otherwise, create
    } else {
      return collectionRef.add(docData);
    }
  }

  /**
   * Return an AngularFirestoreCollection reference
   */
  protected getCollectionReference(): AngularFirestoreCollection {
    return this.afs.collection(this.collectionName);
  }

  /**
   * Extract data from form into object
   * @param fd (Form Data)
   */
  abstract extract(fd);

  /**
   * Implements common items.map method
   * @param items 
   */
  abstract mapItems(items);

  /**
   * Return an empty object (service main object)
   */
  abstract createEmpty(): any;

  /**
   * Get server timestamp
   */
  get timestamp() {
    return firebase.firestore.FieldValue.serverTimestamp();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }
}
