import { Component, OnInit }          from '@angular/core';
import { ActivatedRoute, Router }     from '@angular/router';
import { Observable, combineLatest }  from 'rxjs';
import { map }                        from 'rxjs/operators';

import { PersonaService }             from '../shared/persona.service';

@Component({
  selector: 'app-persona',
  templateUrl: './persona.component.html',
  styleUrls: ['./persona.component.css']
})
export class PersonaComponent implements OnInit {

  // Route id parameter
  id$: Observable<string>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private mainService: PersonaService 
  ) { }

  ngOnInit() {
    this.id$ = combineLatest(
      this.route.params
    ).pipe(
      map(([params]) => params['id'])
    );

    this.id$.subscribe(id => {
      // console.log(id);
      this.mainService.id = id;
    });

    this.mainService.doc$.subscribe(doc => {
      // console.log(doc);
    })
  }

}