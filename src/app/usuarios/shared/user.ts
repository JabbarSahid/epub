export interface User {
  uid?: string;
  displayName?: string;
  email?: string;
  photoURL?: string;
}

export interface UserId extends User {
  id: string;
}
